﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
	public float minSpeed;
	public float maxSpeed;

	void Start()
	{
		rigidbody.velocity = transform.forward * Random.Range(minSpeed, maxSpeed);
	}
}